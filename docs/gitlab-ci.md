# GitLab-CI

## What is continuous integration? {: #ci }

Continuous integration (CI) is the practice of running an automated pipeline of
scripts to build and test a project **after every change**.

This allows maintainers to identify bugs early in the development cycle,
ensuring that all code that is pushed into the `master` development branch
is compliant with the requirements of the project.

!!! info "Continuous deployment"

    Continuous deployment (CD) takes this another step further by automating
    the process of deploying an application to production after every change.

If configured correctly (this is the default), CI pipelines will run for
every merge request, meaning the modified code can be build and tested
**before** changes are accepted into the repository.

The fork-branch-merge-request presented earlier can then be augmented to
include CI at all stages

![GitLab workflow with CI](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)

## How to enable CI on GitLab {: #getting-started }

All Gitlab CI/CD is configured with a YAML-format file called `.gitlab-ci.yml`
in the root of the project repository.

<https://docs.gitlab.com/ee/ci/yaml/README.html>

## Examples

### Running a simple script {: #simple }

A basic example could just check that your script runs without failing.
Consider the example from <https://git.ligo.org/duncanmmacleod/gitlab-example/tree/ci>:

!!! example "Simple Gitlab-CI configuration"
    ```yaml
    image: python

    test:
      script:
        - python hello_world.py
    ```

Here, a single job called `test` is configured to run `python hello_world.py`. If that
command exits with code `0`, the job passes, otherwise it fails.

!!! tip "`image: python`"
    We use `image: python` to declare to GitLab-CI that this job should run inside
    a [`python`](https://hub.docker.com/_/python) container.

    The default is `image: docker:latest`.

An example of this pipeline running is here:

<https://git.ligo.org/duncanmmacleod/gitlab-example/pipelines/83702>

### Building a (python) package {: #python }

A more complicated example is available on the `package` branch of the same
`gitlab-example` repository:

<https://git.ligo.org/duncanmmacleod/gitlab-example/tree/package>

Here the full YAML configuration is:

!!! example "Building and testing a Python package"
    ```yaml
    stages:
      - build
      - test

    image: python

    build:
      stage: build
      script:
        - python setup.py sdist --dist-dir .
      artifacts:
        paths:
          - "hello-world-*.tar.*"

    .test: &test
      stage: test
      dependencies:
        - build
      script:
        - python -m pip install hello-world-*.tar.*
        - hello-world

    test:2.7:
      <<: *test
      image: python:2.7

    test:3.5:
      <<: *test
      image: python:3.5

    test:3.6:
      <<: *test
      image: python:3.6

    test:3.7:
      <<: *test
      image: python:3.7
    ```

Here we define a more complicated pipeline with two stages:

```mermaid
graph TD;
    build-->py27["test:2.7"];
    build-->py35["test:3.5"];
    build-->py36["test:3.6"];
    build-->py37["test:3.7"];
```

The `build` job uses `artifacts` to store the output of its job (the built
tarball) so that it can be used in later jobs.

This configuration also uses a YAML anchor to define a template for the test
jobs, called `.test`, then references it to define the actual test jobs using
the `<<: *test` alias.

An example of this pipeline running is here:

<https://git.ligo.org/duncanmmacleod/gitlab-example/pipelines/83703>

## Other examples

[`lscsoft/gwdatafind`](https://git.ligo.org/lscsoft/gwdatafind/blob/b4cb3fb78767253e3a03f4aedcc140cdbc6adb5b/.gitlab-ci.yml) (python package with automated tests):
: <https://git.ligo.org/lscsoft/gwdatafind/pipelines/61271>

[`lscsoft/bayeswave`](https://git.ligo.org/lscsoft/bayeswave/blob/8d63f87d1123b327916bb40eb7c549bdf41e2043/.gitlab-ci.yml) (cmake build with binary packaging and tests):
: <https://git.ligo.org/lscsoft/bayeswave/pipelines/82380>

[`lscsoft/lalsuite`](https://git.ligo.org/lscsoft/lalsuite/blob/1557872dde72f29aee1d79885440586114971803/.gitlab-ci.yml) (multi-package, multi-distribution package suite with scheduled _nightly_ jobs):
: <https://git.ligo.org/lscsoft/lalsuite/pipelines/81275>

[`emfollow/gwcelery`](https://git.ligo.org/emfollow/gwcelery/blob/e4ab246a0d55fdec34fcb0cc9a0c460cdf43c939/.gitlab-ci.yml) (python package with documentation, tests, and continuous deployment to multiple locations):
: <https://git.ligo.org/emfollow/gwcelery/pipelines/82277>

## GitLab Pages

GitLab supports building static web content with CI and hosting them through
Gitlab with an extension called Pages.

An example of this can be seen for this webpage:

<https://git.ligo.org/duncanmmacleod/gitlab-tutorial/>

For more details, see

<https://about.gitlab.com/product/pages/>
