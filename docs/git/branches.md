# Branches

One of the best ways to use Git is when collaborating with other people on a
single project.
However if lots of people start making changes and pushing their commits,
they might end up overwriting other peoples commits.
Most of the time git will end up not actually allowing you to `push` things
because of conflicts.

To solve this problem we can use **branches**.
A branch is a sequence of commits representing a single line of development.
We refer to the tip of the branch as the **branch head** and it points at a
specific commit.

!!! tip "`master` or `main`"

    The default branch for all new git repositories is called `master`,
    but that is likely to change to `main` in the near future.
    Many git hosting services, including GitLab and GitHub have already
    changed their default branch name to `main`.

Branches are now most commonly used to develop new features without
interrupting the `master` branch (which is typically used for releases),
and are `merged` together once that development task is complete.

## Creating a new branch

We can make our own new branch from the `HEAD` of the current branch
(`master`) using `git branch`):

!!! example "Create branch"

    ```
    git branch dev
    ```

There are lots of ways to use the `git branch` command; you can learn more
at [git-branch](https://git-scm.com/docs/git-branch).

!!! example "List branches"

    ```
    $ git branch --list
    * master
      dev
    ```

By default `git branch` will create the branch, but not shift you onto it,
so you need to `checkout` that branch:

!!! example "Checking out a branch"

    ```
    git checkout dev
    ```

`git status` now tells us that we are on the `dev` branch:


!!! example "Git status (branch)"

    ```
    $ git status
    On branch dev
    nothing to commit, working tree clean
    ```

We can now make a change to our `hello_world.py` script and commit that
to our new branch:

!!! example "Git commit (branch)"

    ```
    echo 'print("My name is Marie")' >> hello_world.py
    git commit hello_world.py -m "hello_world.py: add more info"
    ```

We can now view the history of the repository with a fancy invocation of `git log`

!!! example "Git log (fancy)"

    ```
    $ git log --decorate --oneline --graph --all
    * 7579377 (HEAD -> dev) hello_world.py: add more info
    * 529781f (master) Add hello_world.py
    ```

which shows us that our branches are different, and the relationship between
them.

## Merging branches

Once we have completed our feature development, we can combine the changes
from the `dev` branch, with those on `master`.
To do this, we first move back to the `master` branch

```
git checkout master
```

and then run `git merge` to pull in those changes from the target branch:

!!! example "Git merge"

    ```
    git merge dev
    ```

Because our commits have a direct relationship, the commits are essentially
copied onto the master branch in an operation known as a *fast-forward*.
To learn more about the `git merge` command you can go to
[git-merge](https://git-scm.com/docs/git-merge).
