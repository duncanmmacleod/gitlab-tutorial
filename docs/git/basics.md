# Basics of git

!!! note "Other, better tutorials"

    This tutorial is a short primer inspired by a number of other tutorials,
    including

    - [Version Control with Git](https://swcarpentry.github.io/git-novice/) (Software Carpentry)
    - [Learn GitHub](https://github.com/Galadirith/learn-github) (Ed Fauchon-Jones)

## Configuring git

Once you have `git` installed, the first thing you need to do is tell Git who
you are.
This information is included in every change you make to projects managed with Git:

!!! example "Configuring git for the first time"

    ```shell
    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"
    ```

You can then see the full Git configuration using

```shell
git config --list
```

You can learn more about the `git config` command at
[git-config](https://git-scm.com/docs/git-config).

## Creating a repository

A Git repository is just a directory that includes a special sub-directory called
`.git` that includes the history of changes to the managed files.

A new repository can be initialised simply by creating it and running `git init`

!!! example "Create a new, empty repository"

    ```shell
    mkdir mygitrepo
    cd mygitrepo
    git init
    ```

You can learn more about the git init command at
[git-init](https://git-scm.com/docs/git-init).

You can now check the status of your new repository using `git status`

!!! example "Check the status of a repository"

    ```shell
    git status
    ```

    The output should look something like this:

    ```text
    On branch master

    No commits yet

    nothing to commit (create/copy files and use "git add" to track)
    ```

!!! tip "Run status after every command"

    I find it useful to run `git status` after basically any other Git
    command, to get an overview of the repository status.

You can learn more about the git init command at
[git-status](https://git-scm.com/docs/git-status).

## Creating a commit

We can now create a file to add to our repository.
We can start by creating a Python script that just prints a simple line
of text:

!!! example "Simple python script"

    Create a file called "hello_world.py" with the following contents:

    ```python
    #!/usr/bin/env python3
    print("Hello world!")
    ```

The history that Git stores is a sequence of copies or snapshots of our
project at a specific point in time.
Each of these snapshots is called a **commit**.
A commit is a snapshot of the files in your projects folder that is recorded
by Git along with metadata including when it was committed and who made it.
Everytime we want to create a commit we need to tell Git which files (or parts
of files) we want to save that have been created or changed.
This is done using the `git add` command, you can learn more about it at
[git-add](https://git-scm.com/docs/git-add).

We can now add the file we have just created

!!! example "Adding a new file to the index"

    ```
    git add hello_world.py
    ```

and again we can run `git status` to see what has changed:

!!! example "Git status (`git add`)"

    ```
    $ git status
    On branch master

    No commits yet

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

    	new file:   hello_world.py
    ```

We can run `git add` multiple times to add more files, or pass multiple files
or wildcards to a single `git add` command.
Now that Git knows to track this file, we can create a commit:

!!! example "Committing added changes"

    ```
    git commit -m "Add hello_world.py"
    ```

!!! tip "Writing commit messages in your editor"

    The `-m "<message>"` is optional, and if omitted, Git will open your
    default editor and allow you to enter a message.
    This will be much easier to use if you want to create long and detailed
    commit messaged (spolier: you want to do this).

    See [_Write meaningful commit messages_](#write-meaningful-commit-messages)
    below for some tips on writing good commit messages.

Again, we can run `git status`:

!!! example "Git status (`git commit`)"

    ```
    $ git status
    On branch master
    nothing to commit, working tree clean
    ```

We can now also run `git log` to see the history of the repository:

!!! example "Git log"

    ```
    git log
    ```
    The output will look something like this

    ```text
    commit fac715d3051d16bbadd514afb1e92d039998258c (HEAD -> refs/heads/master)
    Author: Your Name <you@example.com>
    Date:   Fri Oct 11 14:49:55 2019 +0100

        Add hello_world.py
    ```

In Git, all commits are identified by a unique _hash_, and tagged with the name and
email address of the committing user.

## Pushing your commits to a remote location

If you want to share your work, you need to set up a second repository on a
remote machine and connect them.
Relative to your local copy, the other copy (or copies) is called a `remote`, and
may contain commits that you don't have, or vice-versa.

For most users the only remote they connect with is the copy of their repository
on github.com or git.ligo.org, but in general can be any location, including a
location on the same machine.

For demonstration, we can set up a second git repository that can act as a backup
for the primary repository we have already created:

!!! example "Creating a backup repository"

    ```
    git init --bare ../mygitrepo-backup.git
    ```

Not we can add a new `remote` to backup our repository at the given path:

!!! example "Adding a remote"

    ```
    git remote add backup ../mygitrepo-backup.git
    ```

Now that we have connected a remote, we can `push` our commits to the remote
repository:

!!! example "Git push (new branch)"

    ```
    git push backup master
    ```

You can learn more about the `git push` command at [git-push](https://git-scm.com/docs/git-push).

## Cloning a remote repository

When starting to work on an existing repository, the first action will normally
be to `clone` the repository.

`git clone` just creates a complete, local copy of a repository:

!!! example "Git clone"

    ```
    git clone https://git.ligo.org/duncanmmacleod/gitlab-example
    ```

Then `git status` shows us that we are connected to a remote called `origin`
and that the local `master` branch is up-to-date with that on the remote:

!!! example "Git status (git clone)"

    ```
    $ git status
    On branch master
    Your branch is up to date with 'origin/master'.

    nothing to commit, working tree clean
    ```
