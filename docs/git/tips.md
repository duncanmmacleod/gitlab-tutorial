# Good Git practice tips

## Make commits small and independent

Large commits that impact many different files are hard to read, and subsequently
hard to review.
It is better to make commits as small as possible, so that each commit contains
one independent change, even if that is just to a single line in one file.
This also makes changes easy to `revert` if needed;
commits can always be squashed together later if a smaller history footprint is
desired.

## Write meaningful commit messages

For obvious reasons, please do this.

!!! quote "Commit message style guide for Git (<https://commit.style>)"

    Commit message style guide for Git

    The first line of a commit message serves as a summary.  When displayed
    on the web, it's often styled as a heading, and in emails, it's
    typically used as the subject.  As such, you should capitalize it and
    omit any trailing punctuation.  Aim for about 50 characters, give or
    take, otherwise it may be painfully truncated in some contexts.  Write
    it, along with the rest of your message, in the imperative tense: "Fix
    bug" and not "Fixed bug" or "Fixes bug".  Consistent wording makes it
    easier to mentally process a list of commits.

    Oftentimes a subject by itself is sufficient.  When it's not, add a
    blank line (this is important) followed by one or more paragraphs hard
    wrapped to 72 characters.  Git is strongly opinionated that the author
    is responsible for line breaks; if you omit them, command line tooling
    will show it as one extremely long unwrapped line.  Fortunately, most
    text editors are capable of automating this.

## Commit early and often

Because of the distributed nature of Git, there is no downside to committing
as often as possible, even if you wind up modifying or reverting those commits
one minute later.
Committing small changes as soon as you make them (or maybe just after you
test them) will also help prevent data loss, since accidentally overwriting
or deleting a file will only set you back to the last time you committed.

It should be noted that 'commit often' doesn't equate to 'push often', since
once you push there is basically no going back.

## Don't alter the history

In Git it *is* possible to forcibly rewrite the history of a repository,
but this should almost never be exercised in normal usage.
The only good reason to consider this is if you need to remove sensitive
information from the repository (passwords, properietary data).

!!! info "Rebasing"

    It is standard Git practice to 'rebase' a branch, which effectively
    modifies the history to bring it up-to-date relative to another
    branch (normally the `master` branch), but this should only be
    done regularly for feature branches, not for the default branch.

## Don't commit output files

Only source files should be tracked in git. Any files that are generated
by running the source code, can just be regenerated later if needed.

!!! tip "Use `.gitignore`"

    The special `.gitignore` file (either in the root of the repository),
    or hierarchically in sub-directories, can be used to tell git not to
    track files based on `glob`-style regular expressions.

## Don't commit binary files

Git works best for files that can be diffed line-by-line, so doesn't work
very well for binary files such as images.
If you need to commit binary files, especially large binary files, you might
consider using [Git LFS](https://git-lfs.github.com/).
