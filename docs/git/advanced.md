# Advanced git usage

This page includes a number of common, but advanced, git concepts.
These are in no particular order (mostly alphabetical), and are
selection-biased based on my common workflows.

## Adding and committing changes to parts of a file {: #add-patch }

Typically, after modifying a file, you commit it to the repo by running

```
git add file
git commit
```

However, it is possible to only stage specific parts of file, by selecting
lines in the diff that you want to include.

The `--patch` option to `git add` will step through _hunks_ in each file
and ask whether you want to stage each hunk.

## Creating and applying patches without commits {: #git-diff-apply }

It is often useful to create patch files from changes so
that other users can apply them, or so that they can be applied to
distributions.

To create a patch file from an uncommitted change, use `git diff`:

!!! example "Creating a patch file (`git diff`)"

    ```
    git diff > mychange.patch
    ```

You can learn more about the `git diff` command at
[git-diff](https://git-scm.com/docs/git-diff).

To apply a patch file as an uncommitted change, use `git apply`:

!!! example "Applying a patch (`git apply`)"

    ```
    git apply mychange.patch
    ```

You can learn more about the `git apply` command at
[git-apply](https://git-scm.com/docs/git-apply).


## Creating and applying patches with commits {: #git-format-patch-am }

Patches can also be created from complete commits, using `git format-patch`:

!!! example "Creating a patch file (`git format-patch`)"

    ```
    git format-patch -1
    ```

    The argument `-N` will create numered patch files for the most recent
    `N` commits.

You can learn more about the `git format-patch` command at
[git-format-patch](https://git-scm.com/docs/git-format-patch).

Patch files created by `git format-patch` are formatted for submission by
email, and include the original committer ID and messages, so can
be applied by anyone but will keep the original metadata.

To apply commits from patch files, use `git am`:

!!! example "Applying a patch (`git am`)"

    ```
    git am 0001-add-more-info.patch
    ```

You can learn more about the `git am` command at
[git-am](https://git-scm.com/docs/git-am).

## Cherry-picking {: #cherry-pick }

In most circumstances commits are moved from one branch to another
via merging or rebasing the two branches.
However, is it possible to selectively copy commits from one branch
onto another using `git cherry-pick`:

!!! example "Cherry picking commits"

    ```
    git cherry-pick -x <commit-hash>
    ```

You can learn more about the `git cherry-pick` command at
[git-cherry-pick](https://git-scm.com/docs/git-cherry-pick).

This is used regularly, for example, to backport commits from
`master` onto long-term support branches for older releases.
## Large file storage (LFS) {: #lfs }

Git is designed to record the history of files that can be
represented line-by-line, and does not scale very well for large
binary files (compressed data, images, etc).

A third-party extension has been developed to handle versioning of
large files by storing the actual files in a secondary location and
only storing pointers to the files in the main repository history,

This extension is called [Git LFS](https://git-lfs.github.com/), and can
be installed using the package name `git-lfs` on most platforms:

Once the extension is installed, files can be added to tracked using LFS
by using the `git lfs track` command

!!! example "Tracking new files with Git LFS"

    ```
    git lfs track "*.gwf"
    ```

    This will add one or more lines to the `.gitattributes` file in the
    root of the repository, so make sure that this is tracked by Git:

    ```
    git add .gitattributes
    ```

If a repository already has binary files in it, and you would like to
migrate them to LFS, use the `git lfs migrate` command

!!! example "Migrating exiting files to Git LFS"

    ```
    git lfs migrate import --include="*.gwf" --everything --verbose
    ```

!!! warning "Migrating files changes the history"

    `git lfs migrate` will *edit the history of your project*,
    so if you want to push this to a remote, you will need to
    use the `--force` option to convince Git to overwrite the 
    history on the remote

    ```
    git push --force
    ```

    This should be done with **extreme care**, and should be
    coordinated with other repository users, who will likely
    have to take special action (`git reset --hard origin <branch>`)
    to pull in the rewritten history.


## Rebasing {: #rebase }

As an alternative to merging, which creates an extra commitin your repository,
you can `rebase` one branch against another, which will effectively intertwine
the historyies of those two branches.

This strategy works very well when updating a feature branch to pull in the
latest developments on `master`, for example, but should not be used as a direct
alternative to merging feature branches onto `master` as it can create an
inconsistent history.

!!! example "Updating a GitHub/GitLab fork"

    One common use of `git rebase` is to update a fork of a repository
    relative to the canonical upstream remote.
    This should be done before any new feature branches are created:

    ```
    git pull --rebase upstream master
    ```

`git rebase` can also be used to interactively reorder, remove, or combine
commits before pushing to the remote:

!!! example "Modifying local history before pushing"

    ```
    git rebase --interactive master HEAD
    ```

    will open up an editor window showing all commits relative to the
    `master` branch, allowing you to select one of the following

    - `pick` - use commit
    - `reword` - use commit, but edit the commit message
    - `edit` - use commit, but allow amending
    - `squash` - use commit, but meld into the previous commit
    - `fixup` - like `squash`, but discard this commit's log message
    - `drop` - remove commit

    This can be extremely effective to clean up a messy development
    history before merging a new feature from a branch.

You can learn more about the `git rebase` command at
[git-rebase](https://git-scm.com/docs/git-rebase).

## Signing commits {: #gpg }

Because commits are only linked to the committer by means of the email
address, it is relatively easily to spoof a commit.
Commits (and tags) can be GPG signed so that others can verify that
they indeed came from you.

!!! example "Signing a commit"

    ```
    git commit -S
    ```

    will sign a commit with a GPG key.
    You will need to add the relevant signing key to your git config
    with

    ```
    git config --global user.signingkey <GPG_KEY_ID>
    ```

For more details see
[_Git Tools - Signing Your Work_](https://git-scm.com/book/ms/v2/Git-Tools-Signing-Your-Work).

GitHub and GitLab support displaying a '_Verified_' badge alongside commits that
have been signed with a key that they known about. See the following pages
for instructions on how to add the GPG key to your account on that service:

- [GitHub](https://help.github.com/en/articles/adding-a-new-gpg-key-to-your-github-account)
- [GitLab](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)

!!! tip "Signing other actions"

    - `git push` accepts `--signed=if-asked` to sign pushes (if the remote
      server supports that, most don't)
    - `git rebase` accepts `-S` to sign modified commits
    - `git tag` accepts `--sign` to sign tags

## Submodules {: #submodule }

Git allows inclusion of another repository as a subdirectory of the first.
This can be useful to include a javascript library, or other static content,
that is developed somewhere else into a packaged project.

You can learn more about the submodules at
[_Git Tools - Submodules_](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

## Undoing commits 1 (`git reset`) {: #reset }

There are two ways to undo committed changes, the first, `git-reset` should be
used when commits have not been pushed, if the desire is to remove them from
the history.

There are two modes to `git-reset`: `--hard` and `--soft`:

!!! example "Deleting a committed change (`git reset --hard`)"

    ```
    git reset --hard <target>
    ```

    can be used to reset a repository, and remove all commits, back to a
    specific point in the history.

    For example,

    ```
    git reset --hard HEAD^
    ```

    should be used to delete the most recent commit, and discard any changes
    that were included in it.

!!! example "Un-committing a change (`git reset --soft`)"

    ```
    git reset --soft <target>
    ```

    reset's the commit history of a repository back to a point, but leaves
    the files unchanged as 'Changes to be committed' (as `git status` would
    see it)

## Undoing commits 2 (`git revert`)  {: #revert }

The other way to remove commits is to `revert` them, by creating another
commit that undoes the changes in the referenced commit.

!!! example "Reverting a commit"

    ```
    git revert abf563g
    ```

    will create a new commit, with a new unique hash, that undoes the
    changes from commit `abf563g`.
