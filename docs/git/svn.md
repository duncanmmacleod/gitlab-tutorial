# Git for SVN users

## The differences

Git and SVN use fundamentally different models to achieve similar goals.

!!! info "Distributed vs non-distributed"

    Git is a distributed VCS, which means that each `clone` of a repository
    is a complete copy of the remote version, with all branches and history.
    Non-distributed VCS, by comparison, relies on a connection to a remote
    'centralised' copy for almost all actions.

    Distributed VCS has a number of advantages that mainly boil down to speed,
    since most actions are local, and that users can create commits without
    a network connection, or without publishing them to the central server.

## Command comparison

The following table attemps to compare `git` and `svn` commands.

!!! warning "Comparing git and svn commands is hard"

    Because of the distributed vs non-distributed change, certain
    operations in Git have no equivalent in SVN, so comparisons
    like those below should be for informational purposes only,
    and need to be taken with a pinch of salt.

| Command | Operation | Subversion |
| ------- | --------- | ---------- |
| git clone | Copy a repository | svn checkout |
| git commit | Record changes to file history | svn commit |
| git show | View commit details | svn cat |
| git status | Confirm status | svn status |
| git diff | Check differences | svn diff |
| git log | Check log | svn log |
| git add | Add | svn add |
| git mv | Move | svn mv |
| git rm | Delete | svn rm |
| git checkout | Un-stage change | svn revert |
| git reset | Cancel change | svn revert |
| git branch | Make a branch | svn copy |
| git checkout | Switch branch | svn switch |
| git merge | Merge | svn merge |
| git tag | Create a tag | svn copy |
| git pull | Update current branch | svn update (sort of) |
| git fetch | Update local copy of remote | svn update (sort of) |
| git push | It is reflected on the remote | svn commit3 |
| gitignore | Ignore file list | .svnignore |


