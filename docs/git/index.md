# Git

## Overview

Git (<https://git-scm.com/>) is "_a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency_".

What does this mean:

- _free_: no monetary cost
- _open source_: you can look at the code that makes git work (as stored in a git repository...)
- _distributed_: your local copy of a repository contains everything you would get from the remote, so you are not reliant on the server for things like history

What does git allow us to do:

- track changes in files over time
- work collaboratively with many people editing the same file in the same repo at the same time (hopefully not the same lines at the same time)

## Installing git

`git` will be available on all centrally-managed machines, if it isn't there
(i.e. if `which git` (unix) or `where git` (windows) doesn't work, email the
admnistrator and complain).

To install `git` on you own machine:

!!! example "Installing git"

    === "Conda"

        ```shell
        conda install git
        ```

    === "Debian/Ubuntu"

        ```shell
        apt-get install git
        ```

    === "RHEL/Centos/Fedora/Scientific linux"

        ```shell
        yum install git
        ```

    === "Alpine"

        ```shell
        apk add git
        ```

    === "MacPorts (macOS)"

        macOS comes with a pre-installed version of `git`, however you can
        update that with MacPorts:

        ```shell
        port install git
        ```

    === "Homebrew (macOS)"

        macOS comes with a pre-installed version of `git`, however you can
        update that with Homebrew:

        ```shell
        brew install git
        ```

See <https://git-scm.com/downloads> for more instructions on installing git on
other platforms, including Windows.

## Git tutorial

This tutorial is split into a few sections

- [_Basics_](./basics.md)
- [_Branches_](./branches.md)
- [_Advanced concepts_](./advanced.md)
- [_Git for SVN users_](./svn.md)
- [_Good practice tips_](./tips.md)
