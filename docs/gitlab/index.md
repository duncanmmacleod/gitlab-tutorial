# GitLab

!!! info "Summary of git"

    Git is a distributed VCS that is well suited for software development.

    Normally there is a canonical remote version of the repo, stored on a
    server, and users `clone` from there to prepare changes before
    `push`ing commits back to the remote.

GitLab is a service that provides remote repository hosting as well as repository
management tools including

- wikis
- issue tracking
- merge requests
- continuous integration

There is a public instance <https://gitlab.com> that is free to use for
open-source projects, but a number of institutions host there own...

## The IGWN GitLab (<https://git.ligo.org>) {: #git.ligo.org }

The University of Wisconsin-Milwaukee hosts a GitLab instance on behalf of
the IGWN; for more details, including account creation instructions see
<https://computing.docs.ligo.org/guide/services/gitlab/>.

## Why not github.com, or gitlab.com? {: #github}

GitHub and GitLab offer very similar services, each with their own merits.
How you choose where to host a project normally depends very much on personal
preference, rather than the requirements of the project itself, but the
following may come into consideration:

!!! question "When to choose github.com, git.ligo.org, or gitlab.com?"

    - If your project requires contributions from non-IGWN members, you
      should **use github.com or gitlab.com**.

    - If your project contains proprietary IGWN information,
      **use git.ligo.org**.

    - If your project has intensive CI needs, probably **use git.ligo.org**.

    - If you want your project to be maximally visible to the wider public,
      **use github.com**.

## Creating a new project on GitLab {: #creating-a-project }

To create a new project, clik on the `+` icon in the top navigation bar and
select **New project**.

![git.ligo.org new project](./new-project.png)

There is a limit on the number of projects any single user can create, which
(I think) is 25 by default.

### Project naming conventions {: #naming-conventions }

For git.ligo.org there is no official policy regarding naming of groups and
projects, any user is free to create any group they like with any name, and
do whatever they want with it.

It is recommended, however, to self-organise in groups related to the research
area, with sensible naming.
There are a number of established groups that may be appropriate for new
projects to sit under:

| Group | Description |
| ----- | ----------- |
| [`cds`](https://git.ligo.org/cds) | LIGO Lab interferometer control software |
| [`emfollow`](https://git.ligo.org/emfollow`) | EM Follow-up pipeline components and documentation |
| [`lscsoft`](https://git.ligo.org/lscsoft) | (LIGO) Data analysis software and utilities, e.g. [`lalsuite`](https://git.ligo.org/lscsoft/lalsuite), [`gstlal`](https://git.ligo.org/lscsoft/gstlal) |

There are also other groups for drafting [publications](https://git.ligo.org/publications), software [packaging](https://git.ligo.org/packaging), etc.

!!! tip "Subgroups"

    Groups can also nested in a rich hierarchy of subgroups, allowing
    for more sensible grouping of projects, e.g.

    <https://git.ligo.org/publications/O1>  
    <https://git.ligo.org/publications/O2>  
    <https://git.ligo.org/publications/O3>  

## Project wikis {: #wikis }

Each GitLab project comes with a documentation wiki enabled by default.
Each wiki is itself a separate git repository, and so can be modified directly
on the web interface or locally using `git` commands. For more details, see

<https://docs.gitlab.com/ee/user/project/wiki/>

## Issue tracking {: #issues }

Each GitLab project comes with an issue tracker.
This should be the primary medium of reporting issues with any code, or discussing
new features before implementation.
For more details, see

<https://docs.gitlab.com/ee/user/project/issues/>

!!! tip "Use the issue tracker to entice contributions!"

    The issue tracker is the easiest way to inform collaborators, or just
    any interested reader, about a problem, an idea, or a pending change.

    Even if the project is developed by you on your own, posting a ticket
    when you find a bug makes it much less likely that it will get forgotten,
    but gives potential contributors something to take on as their first
    contribution.
    Trivial bugs, code style issues, and refactoring are a few things that
    can be easy for new developers to pick up and have a go at.

### Labels

The issue tracker, as well as merge requests (see below) support adding labels
to tickets to categorise them.
Descriptive titles like `bug` or `feature-request` help identify what sort of
information is contained within, and enable sorting and filtering issues based
on that label.

For more details see

<https://docs.gitlab.com/ee/user/project/labels.html>

!!! tip "Use labels to entice contributions!"

    You can set up labels designed to advertise that issues are low-hanging
    fruit that new contributors should be able to take on and complete.

    Titles like `good-first-issue` or `difficulty::easy` might encourage
    people to try and tackle that issue as a way to get involved in your
    project.

## Project visibility

All projects on GitLab can be set to one of three privacy states

- Private - only project members can view and clone the project
- Internal - (default) project can be viewed and cloned by any logged-in user
- Public - projects can be cloned without any authentication

For more details, see

<https://docs.gitlab.com/ee/public_access/public_access.html>

!!! info "Only logged-in users can contribute"

    Regardless of the visibility of a project, only registered users
    (LIGO.ORG account holders) will be able to interact with your project
    through GitLab (open issues, post merge requests, etc.) - the exception
    to this is [_Service desk_](#service-desk).

## Service desk

GitLab provides a tool to enable email interactions with your project, allowing
non-GitLab members to post support requests to a special email address that
are presented to project members as a ticket on the issue tracker.

For more details, see

<https://about.gitlab.com/product/service-desk/>

!!! example "Service desk example"

    Service desk is already widely used by the EMFollow group to enable public
    feedback on their userguide:

    <https://git.ligo.org/emfollow/userguide/issues/service_desk>

## Merge requests

See [`Merge requests`](merge_requests.md) for more details.
