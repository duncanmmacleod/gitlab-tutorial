# Merge requests

Merge requests are
_the basis of GitLab as a code collaboration and version control platform_
\[[ref](https://docs.gitlab.com/ee/user/project/merge_requests/#overview)].

A merge request is just a way of proposing a change that you have developed
on a separate branch be merged into the default branch (`master`, or `main`).
They enable any number of people to all simultaneously work on the same
project without creating merge conflicts that are hard to resolve
after-the-fact.

!!! tip "Always use forks and merge requests"

    The fork-branch-merge workflow should be used for **all**
    projects, regardless of scale or size.

!!! info "Merge requests and pull requests"

    ...are the same thing, 'Pull request' is just GitHub's name for what GitLab
    calls a 'Merge request'.

## The fork-branch-merge-request workflow

The ideal workflow for development of any bug fix, or new feature, should be:

1. [create a fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
   of the canonical repo

1. clone your fork and add the canonical repo as a `remote`
   ```shell
   git clone git@git.igwn.org:<user>/<name>.git
   git remote add upstream https://git.igwn.org/<group>/<name>.git
   ```

1. create a new feature branch for this feature
   ```shell
   git checkout -b new-feature upstream/master
   ```

1. commit, commit, commit, then push
   ```shell
   git push -u origin new-feature
   ```

1. open a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/)
   to propose these commits be integrated

1. discuss, [review](https://docs.gitlab.com/ee/user/discussions/#merge-request-reviews-premium),
   and (hopefully) merge

1. if you need to update your feature branch relative to upstream changes
   ```shell
   git pull --rebase upstream master
   ```

1. delete the feature branch (using the web interface)

![merge request workflow](https://hackernoon.com/hn-images/1*iHPPa72N11sBI_JSDEGxEA.png)

## Examples {: #merge-requests-examples }

Merge requests on a single-developer project
: <https://git.ligo.org/lscsoft/gwdatafind/merge_requests/18>

Merge requests on a small project
: <https://git.ligo.org/packaging/lscsoft-metapackages/merge_requests/16>

Merge requests on a large project
: <https://git.ligo.org/lscsoft/lalsuite/merge_requests/981>

## Advanced features

### Approvals

Projects can configure themselves to require a formal approval from one or more
eligible contributors.
This is extremely powerful as a way of enforcing code review, and disabling the
ability for merge requests to be submitted and merged by the same person.

<https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html>

This has been configured in [LALSuite](https://git.ligo.org/lscsoft/lalsuite) via
the [Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html) concept,
whereby one or more project contributors are declared in a special file as the 'owner'
of that file; any merge requests that propose changes to those files must be approved
by one of the code owners.

Approvals have been configured in [GWCelery](https://git.ligo.org/emfollow/gwcelery) by
defining groups of contributors, including _peers_, _librarians_, and _review committee_;
one member of each group must approve each merge request.


