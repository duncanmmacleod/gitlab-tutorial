# Git, GitLab, and GitLab-CI

This tutorial attemps to cover the basics of the following tools

- `git` - a distributed version control system (VCS) used predominantly in software development
- `GitLab` - a repository hosting service with extras
- `GitLab-CI` the native continuous integration (CI) service provided with GitLab
