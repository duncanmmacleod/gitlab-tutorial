# Open source software development

!!! danger "This is not legal advice"

    The text on this page is not to be considered legal advice.
    The authors accept no liability relating to the use of the below
    information for any purpose.

Git, GitLab, and GitLab-CI all enable productive software development.
An extra thing to consider is open source development.

!!! info "What is open source?"

    Being 'open source' just means giving the public access to the source
    code for your project and granting them rights to use, modify, and
    redistribute the code under certain conditions.

## Software licensing

The most critical component of open-source development to get right is
understanding how to license your code.

!!! info "What is a software license?"

    A software license grants the end-user permissions to use or
    distribute the software in ways that would otherwise constitute
    copyright infringement.

### No license

By default, if you do not include a license that specifies otherwise, the
work is under exclusive copyright to the copyright holder.
Nobody else can copy, distribute, or modify your work without being at
risk of litigation.
If a work contains multiple contributors (each a copyright holder),
these restrictions may extend to the contributors as well.

!!! danger "Do not use unlicensed software"

    If you come across software that doesn't have a license, **do not use it**.
    You can contact the owners and ask them nicely to add a license.

### What license to use

There are a plethora of open-source licenses available that grant more or less
permissions to the end user.

!!! warning "This is an extreme subset"

    This is a trivial subset of the available licenses, others may be
    immediately more appropriate for a given project based on its content,
    or target audience.

    If in doubt, use the same license as other packages in the same ecosystem,
    or ask your legal counsel.

!!! tip "How to choose a license"

    The website <https://choosealicense.com/> can help pick a license based
    on simple categorisation of your needs.

#### MIT {: #MIT }

The MIT license is probably the simplest, and allows end users to do basically
anything they want with your software whilst protecting the author from
liability.

For more details, see

<https://choosealicense.com/licenses/mit/>

This is used by Pip, Atom, React, and Electron, amongst others.

#### GPL {: #GPL-3.0-or-later }

The GPL license set allows end users to modify and redistribute your code, but
requires them to disclose the source of any larger works using the original
licensed work, and undert the same license.

For more details, see

<https://choosealicense.com/licenses/gpl-3.0/>

This is used by Bash, GIMP, and most other GNU projects.

#### BSD 3-Clause {: #BSD-3-Clause }

Similar to the MIT license, this is a permissive license that prohibits others
from using the name of the project to promote derived products without written
consent.

For more details, see

https://choosealicense.com/licenses/bsd-3-clause/

This is used by Numpy, Scipy, and Astropy, amongst others.

## License compatibility

You may be restricted in which license you can use, based on the license of
the software you depend upon.

For a (fairly) readable summary of software license compatibility, see

<https://www.gnu.org/licenses/license-compatibility.en.html>

## Copyright

The above licensing discussion says nothing of copyright, namely who should
be named as the 'owner' of a work.
Most institutions (including universities) claim ownership over the work of
their staff (I know Cardiff University explicitly does for software), so
please be sure to check the requirements of your employer before slapping a
license on your code.

## Open source vs open development

Producing open-source code **does not** require you to accept contributions
from other people (an 'open development' model).
You are still free to operate in a private development environment on
GitLab (or anywhere else), so long as if you publish your software using
an open-source license, you are able to freely distribute the source code for
that released version.

This does not require you to disclose the development repository, or
its history, or accept feedback of any kind from the community.
